Autonomous drone based on open source Ardupilot software.

Instructions :
https://github.com/imfatant/test
https://github.com/mirkix/ardupilotblue
https://discuss.ardupilot.org/t/beaglebone-blue-ardupilot-blue-real-time-kernel-etc/25530/14
https://github.com/PocketPilot/PocketPilot
https://gitter.im/mirkix/BBBMINI



BOM :

frame (10x8 coeur)
battery 18650
battery 21700 (8.4x7)
FC Blue 9x6
4in1 ESC 5v BEC
antenne
moteur
gps
miscelaneous
battery mount
camera
cooper sheet plate


| Component | Weight (g) |
| ------ | ------ |
| Frame | 200 |
| 4 x 21700 battery cell | 280 |
| Beagle Bone Blue | 40 |
| 4in1 ESC 5V | 6 |
| Antenna | 5 |
| 4x 1806 brushless motor | 72 |
| GPS | 10 |
| Miscelaneous | 50 |
| Camera | 140 |
| Frame | cell |
| ------ | ------ |
| Total | 803 |
